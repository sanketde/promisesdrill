const fs = require('fs');


// promise to read a file
function readFile(filePath) {
    return new Promise((resolve,reject)=>{
        fs.readFile(filePath,'utf-8', (err, data)=>{
            if(err)
            {
                reject(err);
            }
            else
            {
                resolve(data);
            }
        });
    });
}

module.exports = readFile;



