const fs = require('fs');
const { resolve } = require('path');

// promise to change permissions of a file
// this function takes 2 paramenets filepath, permissions
// return a promise
function changeFilePermissions(filepath, permissions)
{
    // create a new promise object
    const promise = new Promise((resolve, reject)=>{
        fs.chmod(filepath, permissions , (err)=>{
            if(err) {
                reject(err)
            }
            else
            {
                resolve();
            }
        });
    });
    return promise;
}

module.exports = changeFilePermissions;