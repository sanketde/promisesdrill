const fs = require('fs');

// promise to check permissions of a file
function checkFilePermissions(filepath)
{
    // create a new promise object
    const promise = new Promise((resolve, reject)=>{
        
        fs.stat(filepath,(err, stats)=>{
            if(err)
            {
                reject(err);
            }
            else
            {
                resolve((stats.mode).toString(8).slice(-3));
            }
        })
    });
    // return the promise
    return promise;
}


// call the checkFile Permissions 
checkFilePermissions('./file.txt')
.then((message)=>{
    console.log(message)
})
.catch((err)=>{
    console.log(err);
})


module.exports = checkFilePermissions;