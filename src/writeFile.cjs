const fs = require('fs');

// promise to write into  a file
function writeFile(filePath, data)
{
    const promise = new Promise((resolve, reject)=>{
        fs.writeFile(filePath,data,'utf8', (err)=>{
            if(err)
            {
                reject(err);
            }
            else{
                resolve();
            }
        });
    });
    
    return promise;
}

module.exports = writeFile;