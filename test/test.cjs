// import the required files
const readFile = require('../src/readFile.cjs');
const writeFile = require('../src/writeFile.cjs');
const changeFilePermissions = require('../src/changeFilePermissions.cjs');
const uuid = require('uuid');


// generate the uuid
const uidv4 = uuid.v4();

// file path
const filepath = './file.txt';


// promise chaining
writeFile(filepath,uidv4)
.then(()=>{
    return readFile(filepath);
})
.then((data)=> {
    // convert the data into upper case
    return data.toUpperCase();
})
.then((dataInUpperCase) =>{
    // write the upper case data into the file
    return writeFile(filepath,dataInUpperCase);
})
.then(()=>{
    // print the message for successful write to the file
    console.log("File written successfully in upperCase");
})
.catch((err) => {
    // print the error
    console.log(err)
});

changeFilePermissions(filepath,'777');

